import os
import sys
import markdown
from abc import abstractmethod, ABC
from bs4 import BeautifulSoup


def main(argv):
    if argv.__len__() < 1 or not argv[0]:
        raise Exception("Path is absent")

    mainDirectory = argv[0]
    if not os.path.exists(mainDirectory):
        raise Exception(f'There is no directory "{mainDirectory}"')

    currentNode = DirectoryNode(getFolderName(mainDirectory))
    tree = Tree(currentNode)

    directoryNodesByPaths = {mainDirectory: currentNode}

    for root, subdirs, files in os.walk(mainDirectory):
        if root not in directoryNodesByPaths:
            currentNode = DirectoryNode(getFolderName(root))
            directoryNodesByPaths[root] = currentNode
        else:
            currentNode = directoryNodesByPaths[root]

        for entry in subdirs:
            header = os.path.join(root, entry)
            subdirNode = DirectoryNode(entry)
            currentNode.nodes.append(subdirNode)
            directoryNodesByPaths[header] = subdirNode

        for entry in files:
            if not entry.endswith('.md'):
                continue

            textNode = TextNode(entry[:-3], readFile(os.path.join(root, entry)))
            directoryNodesByPaths[root].nodes.append(textNode)

    del directoryNodesByPaths

    html = getHtml(tree)
    saveHtml(html)


def parseNode(node):
    html = ""
    if not node.hasBody():
        # its directory node
        html += "<details><summary>"
        html += node.getHeader()
        html += "</summary>"
        for entry in node.getNodes():
            html += parseNode(entry)

        html += "</details>"
    else:
        # its text node
        html += "<details><summary>"
        html += node.getHeader()
        html += "</summary>"
        html += node.getBody()
        html += "</details>"

    return html


def getHtml(tree):
    html = f'<html><head><style type="text/css">{readFile("styles.css")}</style></head><body><div class=\'content\'>'
    html += f'<h1>{tree.root.getHeader()}</h1>'
    for entry in tree.root.getNodes():
        html += parseNode(entry)

    html += "</div></body></html>"
    return html


def readFile(filePath):
    file = open(filePath, "r")
    return file.read()


def saveHtml(content):
    file = open("Walls support helper.html", "w")
    file.write(BeautifulSoup(content, 'html.parser').prettify())
    file.close()


def getFolderName(fullFolderPath: str):
    return os.path.basename(os.path.normpath(fullFolderPath))


class Tree:
    def __init__(self, node):
        self.root = node


class Node(ABC):
    def __init__(self, header):
        self.header = header

    def getHeader(self):
        return self.header

    @abstractmethod
    def getNodes(self):
        ...

    @abstractmethod
    def getBody(self):
        ...

    @abstractmethod
    def hasBody(self):
        ...


class TextNode(Node):
    def __init__(self, header, body):
        super().__init__(header)
        self.body = body

    def getNodes(self):
        return [self]

    def getBody(self):
        return markdown.markdown(self.body)

    def hasBody(self):
        return True


class DirectoryNode(Node):
    def __init__(self, header):
        super().__init__(header)
        self.nodes = []

    def getNodes(self):
        return self.nodes

    def getBody(self):
        return ""

    def hasBody(self):
        return False


if __name__ == '__main__':
    main(sys.argv[1:])
